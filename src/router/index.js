import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import React, { useEffect, useState } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from '../pages/Home';
import Splash from '../pages/Splash';
import LoginScreen from '../pages/Login';
import Register from '../pages/Register';
import ComRegister from '../pages/ComRegister';
import { Provider, useDispatch, useSelector } from 'react-redux';
import store from '../store';
import { Init } from '../actions';
import DetailBook from '../pages/DetailBook';
import PdfShow from '../pages/Pdf'

const Stack = createNativeStackNavigator();

const MyStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DetailBook"
        component={DetailBook}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PdfShow"
        component={PdfShow}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const AuthStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ComRegister"
        component={ComRegister}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const RootNavigation = () => {
  const token = useSelector(state => state.AuthReducers.authToken);
  console.log(token);
  const [loading, setLoading] = useState(true);

  const dispatch = useDispatch();
  const init = async () => {
    await dispatch(Init());
    setLoading(false);
  }

  useEffect(() => {
    init()
  }, [])

  if (loading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems:'center' }}>
        <ActivityIndicator size="large"/>
      </View>
    )
  }

  return token === null ? <AuthStack /> : <MyStack />;
};

const Router = () => {
  return (
    <Provider store={store} >
      <RootNavigation />
    </Provider>
  );
};

export default Router;

const styles = StyleSheet.create({});
