import Logo from './logo.png'
import LoginLogo from './login.png'
import RegisterLogo from './register.png'
import FavLogo from './favorite.png'
import BackLogo from './back.png/'
import ComLogo from './completed.png'
import Book from './a.jpeg'
import StarLogo from './star.png'
import IconShare from './iconShare.png'

export {Logo, LoginLogo, RegisterLogo, FavLogo, BackLogo, ComLogo, Book, StarLogo, IconShare}