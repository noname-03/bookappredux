const initialState = {
  authToken: null,
  userData: {},
  anyData: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state, //copy semua bagian sebelum state
        authToken: action.payload.authToken,
        userData: action.payload.userData,
        anyData: action.payload.anyData,
      };
    case 'LOGOUT':
      return {
        ...state, //copy semua bagian sebelum state
        authToken: null,
      };
    default:
      return state;
  }
};
