import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";

export const Init = () => {
    return async dispatch => {
        let token = await AsyncStorage.getItem('token');
        if (token !== null) {
            console.log('token fetched');
            dispatch({
                type: 'LOGIN',
                payload: token,
            })
        }
    }
}

export const RegisterAction = (name, email, password, { navigation }) => {

    return dispatch => {
        let token = null;
        axios.post('http://code.aldipee.com/api/v1/auth/register', {
            name,
            email,
            password,
        }).then(res => {
            console.log(res)
            navigation.navigate('ComRegister');
            // dispatch({
            //     type: 'REGISTER',
            //     payload: {
            //         'authToken': ApiToken,
            //         'userData': userInfo,
            //         'anyData' : data
            //     },
            // })
        }).catch(e => {
            alert(`error ${e}`)
        })
    }
}

export const Login = (email, password) => {

    return dispatch => {
        let token = null;
        axios.post('http://code.aldipee.com/api/v1/auth/login', {
            email,
            password,
        }).then(res => {
            let data = res.data
            let userInfo = res.data.user
            let ApiToken = data.tokens.access.token
            // console.log(ApiToken)
            //   AsyncStorage.setItem('token', ApiToken);
            dispatch({
                type: 'LOGIN',
                payload: {
                    'authToken': ApiToken,
                    'userData': userInfo,
                    'anyData': data
                },
            })
        }).catch(function (error) {
            if (error.response) {
                alert(error.response.data.message)
            }
        })
    }
}



export const Logout = () => {
    return async dispatch => {
        await AsyncStorage.clear();
        dispatch({
            type: 'LOGOUT'
        })
    }
}