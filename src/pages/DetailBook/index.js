import { View, Text, Image, Button, Alert, ScrollView, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useSelector } from 'react-redux'
import { BackLogo, FavLogo, IconShare } from '../../assets'
import Rating from 'react-native-easy-rating'
import { ColorPrimary, ColorSecondary, ColorTertiary } from '../../utils'
// import Share from 'react-native-share'

const DetailBook = ({ route, navigation }) => {

    const token = useSelector(state => state.AuthReducers.authToken)
    const [Data, setData] = useState([])
    const [Price, setPrice] = useState([])
    const { id } = route.params;
    console.log(id)
    console.log(token)

    function back() {
        navigation.navigate('Home')
    }

    const movieDetail = async () => {
        try {
            const results = await axios.get(
                `http://code.aldipee.com/api/v1/books/${id}`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                },
            );
            setData(results.data);
            setPrice(results.data.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'))
        } catch (err) {
            Alert.alert('ERROR MESSAGE', `${err}`);
            setTimeout(() => {
                alert(err.response.data.message)
                back()
            }, 1000);
        }
    };


    useEffect(() => {
        movieDetail();
        // getApiRating();
        console.log(Data)
        // console.log(anyInfo)
    }, []);


    return (
        <View style={{ flexDirection: 'column', backgroundColor: (ColorPrimary) }}>
            <ScrollView>
                <View style={{ marginHorizontal: 12, marginBottom: 12, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: ColorSecondary, borderRadius: 25 }}>
                    <View style={{ marginTop: 8, marginBottom: 8, marginLeft: 8 }}>
                        <TouchableOpacity onPress={() => {
                            navigation.navigate('Home')
                        }}>
                            <Image source={BackLogo} style={{ width: 25, height: 25 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 8, marginBottom: 8, width: 280 }}>
                        <Text numberOfLines={1} style={{ fontSize: 18, fontWeight: 'bold' }}>{Data.title}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginRight: 8, marginTop: 8, marginBottom: 8 }}>
                        <Image source={FavLogo} style={{ width: 25, height: 25 }} />
                        <TouchableOpacity>
                            <Image source={IconShare} style={{ width: 25, height: 25 }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', margin: 12, backgroundColor: ColorSecondary, borderRadius: 25 }}>
                    <View>
                        <TouchableOpacity onPress={() => {
                            navigation.navigate('PdfShow')
                        }}>
                        <Image source={{ uri: `${Data.cover_image}` }} style={{ margin: 12, width: 100, height: 150 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ margin: 12, flexWrap: 'wrap', flexDirection: 'column', width: 240, justifyContent: 'space-around' }}>
                        <View>
                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{Data.title}</Text>
                        </View>
                        <View>
                            <Text>{Data.author}</Text>
                        </View>
                        <View>
                            <Text>{Data.publisher}</Text>
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around', backgroundColor: ColorSecondary, borderRadius: 25, margin: 12 }}>
                    <View style={{ flexDirection: 'column', margin: 12 }}>
                        <Text style={{ fontSize: 18 }}>Rating</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Rating
                                rating={Data.average_rating}
                                max={1}
                                value={5}
                                iconWidth={30}
                                iconheight={10}
                            />
                            <Text style={{ fontSize: 16, margin: 6 }}>{Data.average_rating}</Text>
                        </View>
                    </View>
                    <View style={{ margin: 12, flexDirection: 'column' }}>
                        <Text style={{ fontSize: 18 }}>Total Sale</Text>
                        <Text style={{ fontSize: 16, marginTop: 6 }}>{Data.total_sale}</Text>
                    </View>
                    <View style={{ justifyContent: 'center' }}>
                        <TouchableOpacity style={{ backgroundColor: ColorTertiary, width: 160, height: 40, borderRadius: 25 }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', marginLeft: 24, marginTop: 8 }}>Buy Rp.{Price}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Text style={{ margin: 12, fontWeight: 'bold', fontSize: 24 }}>Overview</Text>
                <View style={{ margin: 12, backgroundColor: ColorSecondary, borderRadius: 25 }}>
                    <Text style={{ textAlign: 'justify', margin: 12, fontSize: 16 }}>{Data.synopsis}</Text>
                </View>
            </ScrollView>
        </View>
    )
}

export default DetailBook