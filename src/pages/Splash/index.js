import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'
import { useEffect } from 'react'
import { Logo } from '../../assets'
import { ColorPrimary,ColorSecondary,ColorTertiary } from '../../utils'

const Splash = ({ navigation }) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('LoginScreen')
        }, 3000)
    }, [navigation])
  return (
      <View style={{ flex:1, justifyContent: 'center', alignItems:'center', backgroundColor:ColorPrimary}}>
          <Image source={Logo} style={{  height:100, width:90}} />
          <Text style={{ fontWeight: 'bold', fontSize: 24}}>
              Free E-Book
          </Text>

          <Text style={{ top: 200, fontWeight:'bold' }}>
              FAHRURROZI - UCIC
          </Text>
      </View>
  )
}

export default Splash

const styles = StyleSheet.create({})