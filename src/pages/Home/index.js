import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity, RefreshControl } from 'react-native'
import React, { useEffect, useState } from 'react'
import Rating from 'react-native-easy-rating'
import { useDispatch, useSelector } from 'react-redux'
import { Logout } from '../../actions'
import axios from 'axios'
import { BackLogo } from '../../assets'
import { ColorPrimary, ColorSecondary, ColorTertiary } from '../../utils'

function Home({ navigation }) {

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        wait(500).then(() => setRefreshing(false), getApi(), getApiRating());
    }, []);

    const token = useSelector(state => state.AuthReducers.authToken)
    const userInfo = useSelector(state => state.AuthReducers.userData)
    const anyInfo = useSelector(state => state.AuthReducers.anyData)
    const [Data, setData] = useState([]);
    const [ratingBook, setRatingBook] = useState([]);
    const dispatch = useDispatch()
    const logout = () => {
        dispatch(Logout())
    }

    const getApi = async () => {
        try {
            const post = await axios.get('http://code.aldipee.com/api/v1/books/',
                {
                    headers: {
                        "Authorization": `Bearer ${token}`
                    }
                }
            );
            setData(post.data.results);
        } catch (error) {
            console.log(error)
            setTimeout(() => {
                alert(error.response.data.message)
                logout()
            }, 1000);
        }
    };
    const getApiRating = async () => {
        try {
            const post = await axios.get('http://code.aldipee.com/api/v1/books/',
                {
                    headers: {
                        "Authorization": `Bearer ${token}`
                    }
                }
            );
            setRatingBook(post.data.results.sort((a, b) => b.average_rating - a.average_rating).slice(0, 6));
        } catch (error) {
            console.log(error)
            setTimeout(() => {
                alert(error.response.data.message)
                logout()
            }, 1000);
        }
    };



    useEffect(() => {
        getApi();
        getApiRating();
        console.log(Data)
        console.log(anyInfo)
    }, []);


    return (
        <View style={{ flexDirection: 'column', backgroundColor: '#169a9c' }}>
            <ScrollView refreshControl={
                <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            }>
                <View style={{ marginHorizontal: 12, marginBottom: 12, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: ColorSecondary, borderRadius: 25 }}>
                    <View style={{margin:8}}>
                        <TouchableOpacity onPress={logout}>
                            <Image source={BackLogo} style={{ width: 25, height: 25 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{margin:8, flexDirection: 'row', marginRight:8 }}>
                        <Text style={{ fontSize: 16 }}>Welcome, </Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{userInfo.name}</Text>
                    </View>
                </View>
                <View>
                    <Text style={{ margin: 12, fontWeight: 'bold', fontSize: 16 }}>Recommended</Text>
                    <ScrollView horizontal={true}>
                        {Data.map((data, index) => (
                            <TouchableOpacity
                                onPress={() => {
                                    navigation.navigate('DetailBook', {
                                        id: `${data.id}`,
                                    })
                                }}
                                key={index}>

                                <View style={{ justifyContent: 'center', alignContent: 'center', display: 'flex', margin: 12, width: 120, height: 200, backgroundColor: '#bde7ed', borderRadius: 25 }}>
                                    <View style={{ margin: 10 }}>
                                        <Image source={{ uri: `${data.cover_image}` }} style={{ width: 100, height: 120, borderRadius: 12 }} />
                                        <Text numberOfLines={1} style={{ fontWeight: 'bold' }}>{data.title}</Text>
                                        <Text numberOfLines={1} style={{ fontSize: 8 }}>{data.author}</Text>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text numberOfLines={1} style={{ fontSize: 8, fontWeight: 'bold' }}>Rp. {data.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</Text>
                                            <Rating style={{ marginLeft: 4 }}
                                                rating={data.average_rating / 2}
                                                max={5}
                                                iconWidth={9}
                                                iconHeight={9} />
                                        </View>
                                    </View>
                                </View>

                            </TouchableOpacity>
                        ))}
                    </ScrollView>
                </View>
                <View>
                    <Text style={{ margin: 12, fontWeight: 'bold', fontSize: 16 }}>Popular Book</Text>

                    <View style={{ flexDirection: 'column', backgroundColor:'#76ceda', marginHorizontal:4, borderRadius:12 }}>

                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', width: 440 }}>
                            {ratingBook.map((bestData, index) => (
                                <TouchableOpacity
                                onPress={() => {
                                    navigation.navigate('DetailBook', {
                                        id: `${bestData.id}`,
                                    })
                                }}
                                key={index}>

                                    <View style={{ justifyContent: 'center', alignContent: 'center', display: 'flex', margin: 9, width: 114, height: 200, backgroundColor: '#bde7ed', borderRadius: 25 }}>
                                        <View style={{ margin: 10 }}>
                                            <Image source={{ uri: `${bestData.cover_image}` }} style={{ width: 94, height: 120 }} />
                                            <Text numberOfLines={1} style={{ fontWeight: 'bold' }}>{bestData.title}</Text>
                                            <Text numberOfLines={1} style={{ fontSize: 8 }}>{bestData.author}</Text>
                                            <Text numberOfLines={1} style={{ fontSize: 8 }}>{bestData.publisher}</Text>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Text numberOfLines={1} style={{ fontSize: 8, fontWeight: 'bold' }}>Rp. {bestData.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</Text>
                                                <Rating style={{ marginLeft: 4 }}
                                                    rating={bestData.average_rating / 2}
                                                    max={5}
                                                    iconWidth={9}
                                                    iconHeight={9} />
                                            </View>
                                        </View>
                                    </View>

                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default Home

const styles = StyleSheet.create({})