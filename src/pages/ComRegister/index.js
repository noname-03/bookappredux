import { View, Text, Image, Button, TouchableHighlight } from 'react-native'
import React from 'react'
import { ComLogo } from '../../assets'

const ComRegister = ({navigation}) => {
    return (
        <View style={{ alignItems: 'center' }}>
            <Text style={{ top: 40, margin: 24, fontSize: 30, fontWeight: 'bold' }}>
                Registration Completed!
            </Text>
            <Image source={ComLogo} style={{ top: 120, width: 100, height: 100 }} />
            <Text style={{ top: 150, margin: 24, fontSize: 30, fontWeight: 'bold', textAlign: 'center' }}>
                We sent email verification to your email
            </Text>
            <TouchableHighlight
            onPress={() => {
                navigation.navigate('LoginScreen')
            }}
                style={{ top:190, paddingLeft:60,paddingRight:60, paddingTop: 20, paddingBottom: 20, backgroundColor: 'blue', borderRadius: 10, borderWidth: 1, borderColor: '#fff', }} underlayColor='#fff'>
                <Text style={{ color: '#fff', textAlign: 'center' }}>Back to login</Text>
            </TouchableHighlight>
        </View>
    )
}

export default ComRegister