import { View, Text, Image, Dimensions, TextInput, Button, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import { BackLogo, RegisterLogo } from '../../assets'
import { useDispatch } from 'react-redux';
import axios from 'axios';
import { ColorPrimary,ColorSecondary } from '../../utils';

const Register = ({ navigation }) => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const dispatch = useDispatch()
    const submit = () => {
        // dispatch(RegisterAction(name, email, password))
        axios.post('http://code.aldipee.com/api/v1/auth/register', {
            name,
            email,
            password,
        }).then(res => {
            console.log(res)
            navigation.navigate('ComRegister');
        }).catch(function (error) {
            if (error.response) {
                alert(error.response.data.message)
            //   console.log(error.response);
            //   console.log(error.response.data.message);
            //   console.log(error.response.status);
            //   console.log(error.response.headers);
            }
        })
    };
    return (
        <View style={{ flex: 1, backgroundColor:ColorPrimary }}>
            <TouchableOpacity onPress={() => {
                navigation.navigate('LoginScreen')
            }}>
                <Image source={BackLogo} style={{ margin: 12, width: 25, height: 25 }} />
            </TouchableOpacity>
            <Image source={RegisterLogo} style={{ margin: 12, width: 400, height: 200 }} />
            <TextInput
                style={{ margin: 12, padding: 10, borderWidth: 1 }}
                placeholder="Name"
                value={name}
                onChangeText={(text) => setName(text)}
            />
            <TextInput
                style={{ margin: 12, padding: 10, borderWidth: 1 }}
                placeholder="Email"
                value={email}
                onChangeText={(text) => setEmail(text)}
            />
            <TextInput
                style={{ margin: 12, padding: 10, borderWidth: 1 }}
                placeholder="Password"
                secureTextEntry={true}
                value={password}
                onChangeText={(text) => setPassword(text)}
            />
            <View style={{ margin: 12 }}>
                <Button onPress={submit} title="Register" color={ColorSecondary} />
            </View>
            <View style={{ alignItems: 'center' }}>
                <Text style={{ alignItems: 'center' }}>Already have an account ?</Text>
                <TouchableOpacity onPress={() => {
                    navigation.navigate('LoginScreen')
                }}>
                    <Text style={{ color: 'blue', fontWeight: 'bold', fontSize: 16 }}>
                        Login
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Register