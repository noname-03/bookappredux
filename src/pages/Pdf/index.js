import { View, Text, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import Pdf from 'react-native-pdf'
import { ColorPrimary, ColorSecondary, ColorTertiary } from '../../utils'
import { BackLogo, FavLogo, IconShare } from '../../assets'

const PdfShow = ({navigation}) => {

    return (
        <View style={{display:'flex', flexDirection: 'column', backgroundColor: (ColorPrimary) }}>
            <View style={{ marginHorizontal: 12, marginBottom: 12, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: ColorSecondary, borderRadius: 25 }}>
                <View style={{ marginTop: 8, marginBottom: 8, marginLeft: 8 }}>
                    <TouchableOpacity onPress={() => {
                        navigation.navigate('Home')
                    }}>
                        <Image source={BackLogo} style={{ width: 25, height: 25 }} />
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', marginRight: 8, marginTop: 8, marginBottom: 8 }}>
                    <Image source={FavLogo} style={{ width: 25, height: 25 }} />
                    <TouchableOpacity>
                        <Image source={IconShare} style={{ width: 25, height: 25 }} />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ alignItems:'center'}}>
                <Pdf source={{ uri: 'http://samples.leanpub.com/thereactnativebook-sample.pdf' }} style={{ width: 340, height: 600 }} />
            </View>
        </View>
    )
}

export default PdfShow