import {
    View,
    Text,
    Image,
    Dimensions,
    TextInput,
    Button,
    TouchableOpacity,
} from 'react-native';
import React, { useState } from 'react';
import { LoginLogo } from '../../assets';
import { Login } from '../../actions';
import { useDispatch } from 'react-redux';
import { ColorPrimary,ColorSecondary,ColorTertiary } from '../../utils';

const LoginScreen = ({ navigation }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const dispatch = useDispatch()
    const submit = () => {
        dispatch(Login(email, password))
    };

    return (
        <View style={{ flex: 1, backgroundColor:ColorPrimary }}>
            <Image source={LoginLogo} style={{ margin: 12, width: 400, height: 200 }} />
            <TextInput
                style={{ margin: 12, padding: 10, borderWidth: 1 }}
                placeholder="Email"
                value={email}
                onChangeText={(text) => setEmail(text)}
            />
            <TextInput
                style={{ margin: 12, padding: 10, borderWidth: 1 }}
                placeholder="Password"
                secureTextEntry={true}
                value={password}
                onChangeText={(text) => setPassword(text)}
            />
            <View style={{ margin: 12 }}>
                <Button onPress={submit} title="Login" color={ColorSecondary} />
            </View>
            <View style={{ alignItems: 'center' }}>
                <Text style={{ alignItems: 'center' }}>Don't have an account ?</Text>
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate('Register');
                    }}>
                    <Text style={{ color: 'blue', fontWeight: 'bold', fontSize: 16 }}>
                        Register
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default LoginScreen;
