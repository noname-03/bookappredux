import 'react-native'
import React from 'react'
import Home from '../src/pages/Home'
import renderer from 'react-test-renderer'

import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock';


test('Home Snapshot', () => { 
    const snap = renderer.create(
        <Home/>
    ).toJSON()
    expect(snap).toMatchSnapshot()
 })

// jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);